package com.mailinator;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;



public class MailUtil {


    private static String USER_AGENT ="Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0";
    private static String cookie="";

    public static JSONObject sendGet(String url,HashMap <String,String> map) throws JSONException {
        HttpResponse<String> res = null;
        try {

            String data = "";
            for(String key : map.keySet()){

                data+=key+"="+map.get(key)+"&";


            }

            if(Mailinatot.log) {
                System.out.println("Request = " +url+data );
            }

            res = Unirest.get(url + data)
                    .header("User-Agent", USER_AGENT)

                    .header("cookie", cookie)


                    .asString();


        } catch (UnirestException e) {
            e.printStackTrace();
        }

        String resBody = res.getBody();
        //Set cookies to request
        if (res.getHeaders().get("Set-Cookie") !=null ) {

            List<String> cookies = res.getHeaders().get("Set-Cookie");
            for (int i =0;i<cookies.size();i++){

                StringTokenizer cut = new StringTokenizer(cookies.get(i),";");

                cookie +=cut.nextToken()+"; ";
            }
        }

        if(Mailinatot.log) {
            System.out.println("Respons = " + resBody);
        }
        return new JSONObject(resBody);
    }

}
