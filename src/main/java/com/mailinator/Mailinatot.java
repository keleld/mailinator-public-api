package com.mailinator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class Mailinatot {


    private static String INBOX = "https://www.mailinator.com/api/webinbox2?";
    private static String MAIL= "https://www.mailinator.com/fetchmail?";
    private String email;
    private static int x = 0;
    public static boolean log = false;
    private JSONObject msgs;

    public Mailinatot(String email) {
        this.email = email ;
    }

    public Mailinatot (String email, boolean log){
        this.email = email;
        this.log = log;

    }

    public JSONObject getInboxMsgs () throws JSONException {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("public_to", email);
        map.put("x","0");

        msgs = MailUtil.sendGet(INBOX, map);
        return msgs;

    }

    public JSONObject getMsgById(String msgId) throws JSONException {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("zone","public");
        map.put("msgid", msgId);

        return  MailUtil.sendGet(MAIL ,map);

    }

    public JSONArray getMsgsBySender (String senderEmail) throws JSONException {
        JSONArray msgArr =  msgs.getJSONArray("public_msgs");
        JSONArray arr = new JSONArray();
        for(int i = 0;i<msgArr.length();i++){
            if(msgArr.getJSONObject(i).getString("from").equals(senderEmail)){
                arr.put(msgArr.getJSONObject(i));

            }
        }
        return arr;
    }

    public JSONArray getMsgsBySubject (String subject) throws JSONException {
        JSONArray msgArr =  msgs.getJSONArray("public_msgs");
        JSONArray arr = new JSONArray();
        for(int i = 0;i<msgArr.length();i++){
            if(msgArr.getJSONObject(i).getString("subject").equals(subject)){
                arr.put(msgArr.getJSONObject(i));

            }
        }
        return arr;
    }



    private String getX() {
        x++;
        return String.valueOf(x);
    }
}
